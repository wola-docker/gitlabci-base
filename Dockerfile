FROM ubuntu:20.04

RUN apt update && \
    apt install -y openssh-client git unzip sshpass rsync --fix-missing && \
    apt-get clean autoclean && \
    apt-get autoremove --yes && \
    rm -rf /var/lib/{apt,dpkg,cache,log}/

